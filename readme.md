# **Driver board** #

* MCU Atmel **AT90CAN128-16AU**
* False power connection protection
* 1x H bridge VNH2SP30-E with encoder connector
* 1x CAN bus communication
* 2x High current outputs IRLR120
* 4x Servo motor connectors
* 3x Servo motor connectors **RX-24F**
* 3x Servo motor connectors **AX-12**

![24032178369_366b273de7_k.jpg](https://bitbucket.org/repo/M44RRA/images/3121110858-24032178369_366b273de7_k.jpg)